import 'package:flutter/material.dart';

var btnDefaultTheme = ElevatedButton.styleFrom(
    primary: Colors.white, // background color
    side: BorderSide(width: 1.0, color: Colors.white),
    onPrimary: Colors.white// text color
);