import 'package:csp_flutter/utils/functions.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '/utils/api.dart';
import '/providers/user_provider.dart';
import '/models/project.dart';
import '/widgets/project_card.dart';
import '/widgets/app_drawer.dart';
import '/widgets/add_project_dialog.dart';

class ProjectListScreen extends StatefulWidget {
    @override
    ProjectListScreenState createState() => ProjectListScreenState();
}

class ProjectListScreenState extends State<ProjectListScreen> {
    Future<List<Project>>? _futureProjects;

    void showAddTaskDialog(BuildContext context){ 
        showDialog(
            context: context,
            builder : (BuildContext context) => AddProjectDialog()
        ).then((value){
            final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken;    
        });
    }

    final _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

    void _reloadProjects() {
        final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken; 
        setState(() {
            _futureProjects = API(accessToken).getProjects().catchError((error) {
                showSnackBar(context, error.message);
            });
        });
    }   

    Widget showProjects(List? projects) {
        var cardProjects = projects!.map((project) => ProjectCard(project, _reloadProjects)).toList();

        return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () async {
                _reloadProjects();
            },
            child: ListView(
                children: cardProjects
            ), 
        );
    }

    @override
    void initState() {
        super.initState();

        WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
            final String? accessToken = context.read<UserProvider>().accessToken;
            print('apc $accessToken');

            setState(() {
            _futureProjects = API(accessToken).getProjects().catchError((error) {
                showSnackBar(context, error.message);
            });
            });
        });
    }

    @override
    Widget build(BuildContext context) {
        final String? designation = Provider.of<UserProvider>(context).designation;

        Widget fabAddProject = FloatingActionButton(
            child: Icon(Icons.add),
            backgroundColor: Colors.red,
            foregroundColor: Colors.white,
            onPressed: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) => AddProjectDialog()
                ).then((value) {
                    return AlertDialog();
                });
            },
        );

        Widget projectListView = FutureBuilder(
            future: _futureProjects,
            builder: (context, snapshot) {
                if (snapshot.hasData) {
                    return showProjects(snapshot.data as List);
                } 
                return Center(
                child: CircularProgressIndicator()
            );
        }
    );

        return Scaffold(
            appBar: AppBar(title: Text('Project List',
            style: TextStyle(
                color: Colors.white)
            ),
            
            ),
            endDrawer: AppDrawer(),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: projectListView
            ),
            floatingActionButton: (designation == 'contractor') ? fabAddProject : null,
        );
    }
}