import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '/providers/user_provider.dart';
import '/models/task.dart';
import '/widgets/task_card.dart';

class TaskListScreen extends StatefulWidget {
    final int? projectId;

    TaskListScreen(this.projectId);

    @override
    _TaskListScreen createState() => _TaskListScreen();
}

class _TaskListScreen extends State<TaskListScreen> {
    Future<List<Task>>? _futureTasks;

    final _refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

    void _reloadTasks() {

    }

    Widget showTasks(List? tasks) {
        var cardTasks = tasks!.map((task) => TaskCard(task, _reloadTasks)).toList();

        return RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: () async {
                _reloadTasks();
            },
            child: ListView(
                children: cardTasks
            ), 
        );
    }

    @override
    void initState() {
        super.initState();

        WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {

        });
    }

    @override
    Widget build(BuildContext context) {
        final String? designation = Provider.of<UserProvider>(context).designation;

        Widget fabAddTask = FloatingActionButton(
            child: Icon(Icons.add),
            backgroundColor: Color.fromRGBO(205, 23, 25, 1),
            foregroundColor: Colors.white,
            onPressed: () {

            },
        );

        Widget taskListView = FutureBuilder(
            future: _futureTasks,
            builder: (context, snapshot) {
                if (snapshot.hasData) {
                    return Container();
                } else {
                    return Center (
                        child: CircularProgressIndicator()
                    );
                }
            }
        );

        return Scaffold(
            appBar: AppBar(title: Text('Project Task List')),
            body: Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: taskListView
            ),
            floatingActionButton: (designation == 'subcontractor') ? fabAddTask : null
        );
    }
}