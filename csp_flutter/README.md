# Flutter Capstone Information

# Packages

**http: ^0.13.3**

     A composable, multi-platform, Future-based API for HTTP requests.

**provider: ^5.0.0**

     A wrapper around InheritedWidget to make them easier to use and more reusable.

**shared_preferences: ^2.0.6**

     Flutter plugin for reading and writing simple key-value pairs. Wraps NSUserDefaults on iOS and SharedPreferences on Android.

**image_picker: ^0.8.0+3**

     Flutter plugin for selecting images from the Android and iOS image library, and taking new pictures with the camera.

**flutter_dotenv: ^5.0.0**

     Easily configure any flutter application with global variables using a `.env` file.

**email_validator: '^2.0.1'**

     A simple (but correct) dart class for validating email addresses.

# Flutter version

Flutter: 2.2.3

# Features of the Project

**Login Feature**

- Save token to app state
- Save token to local storage

**Contractor**

- Show all projects
- Add project
- Assign project to subcontractor
- Review finished project tasks

**Subcontractor**

- Show assigned projects
- Show project tasks
- Add task & assign to assembly team

**Assembly Team**

- Show projects with assigned tasks
- Show project tasks
- Tag task as ongoing
- Tag task as completed

# Set up Guide

1. Click the button labeled as Clone from the repository page.
2. In the drop down menu, select SSH.
3. Copy the URL for the repository.
4. On your local machine, open the bash shell and change the directory to your own working directory where you would like to clone the repository.
5. Paste the link that you copied from your repository.
6. Press enter, and the repository will be stored in your working directory.

# Test User Credentials

[Untitled](https://www.notion.so/fc410656c15b4933bffaf9851291c2ec)