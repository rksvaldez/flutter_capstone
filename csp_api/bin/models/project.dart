import 'package:sqlite3/sqlite3.dart';

import '../portable_database.dart';

class Project {
    static bool add(String name, String description, int userId) {
        try {
            final PreparedStatement statement = db.prepare('INSERT INTO projects (name, description, createdBy) VALUES (?, ?, ?)');

            statement.execute([name, description, userId]);

            return true;
        } catch (e) {
            print(e.toString());
            return false;
        }
    }

    static dynamic retrieve(int userId, String userDesignation) {
        try {
            ResultSet projects;

            if (userDesignation == 'contractor') {
                projects = db.select('SELECT * FROM projects WHERE createdBy = ?', [userId]);
            } else if (userDesignation == 'subcontractor') {
                projects = db.select('SELECT * FROM projects WHERE assignedTo = ?', [userId]);
            } else if (userDesignation == 'assembly-team') {
                projects = db.select('SELECT * FROM projects WHERE id IN (SELECT DISTINCT projectId FROM tasks WHERE assignedTo = ?)', [userId]);
            }

            if (projects == null) {
                return List.empty();
            }

            return (projects == null) ? List.empty() : projects.toList();
        } catch (e) {
            print(e.toString());
            return { 'error': 'Project.retrieve() encountered an error.' };
        }
    }

    static bool assign(int assignedTo, int projectId) {
        try {
            final PreparedStatement statement = db.prepare('UPDATE projects SET assignedTo = ? WHERE id = ?');

            statement.execute([assignedTo, projectId]);
            
            return true;
        } catch (e) {
            print(e.toString());
            return false;
        }
    }
}